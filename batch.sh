#!/bin/bash

TEXT="Hello Batch Variable!"

BASE_DIR=$(pwd)
VIDEO_DIR=workshop/batch/videos
SHADER_DIR=workshop/batch/shaders
RENDER_DIR=workshop/batch/render

echo ${TEXT}

echo "Base directory: ${BASE_DIR}"
echo "Video directory: ${BASE_DIR}/${VIDEO_DIR}"
echo "Shader directory: ${BASE_DIR}/${SHADER_DIR}"

if [ ! -d ${VIDEO_DIR} ]; then
  echo "${VIDEO_DIR} does not exist"
  exit
fi

if [ ! -d ${SHADER_DIR} ]; then
  echo "${SHADER_DIR} does not exist"
  exit
fi

VIDEO_LIST=$(ls ${VIDEO_DIR})
echo ${VIDEO_LIST}

SHADER_LIST=$(ls ${SHADER_DIR})
echo ${SHADER_LIST}

for VID in ${VIDEO_LIST}; do
  for SHD in ${SHADER_LIST}; do
    #echo "Rendering video ${VID} with shader ${SHD}"

    VID_FULL=$(basename -- ${VID})
    VID_PLAIN="${VID_FULL%.*}"

    SHD_FULL=$(basename -- ${SHD})
    SHD_PLAIN="${SHD_FULL%.*}"

    #echo "Video name w/o extension: ${VID_PLAIN}"
    #echo "Shader name w/o extension: ${SHD_PLAIN}"

    NEW_VIDEO_NAME="${VID_PLAIN}_${SHD_PLAIN}.mp4"
    echo "Rendering new file name: ${NEW_VIDEO_NAME}"

    ./ffmpeg -hide_banner -i "${VIDEO_DIR}/${VID}" \
      -r 25 -vf "shadertoy=${SHADER_DIR}/${SHD}" \
      -c:v libx264 -preset fast -crf 25 \
      -f mp4 -c:a copy -y \
      "${RENDER_DIR}/${NEW_VIDEO_NAME}"
  done
done
