# ffmpeg_shadertoy_workshop

Welcome to the ffmpeg_shadertoy_workshop. We are going to use a custom version of ffmpeg to render videos with GLSL shaders from shadertoy.

## Downloads

- [Video samples](https://www.dropbox.com/sh/gin4o4jok4n9huf/AADQ-to5oiJaFUQeqTPIfZqsa?dl=0)
- [Batch script](batch.sh)
- [Join script](join.sh)

## Links

- [Pi Mapper](https://ofxpimapper.com/)
- [Pi VLC](https://kriwkrow.gitlab.io/pivlc/)

## Licence

Attribution-NonCommercial-ShareAlike 4.0 International ([CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/))
