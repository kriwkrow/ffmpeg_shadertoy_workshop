#!/bin/bash

BASE_DIR=$(pwd)
VIDEO_DIR=workshop/batch/selection
INTER_DIR=workshop/batch/intermediates
OUTPUT_DIR=workshop/batch

# 1. Convert all selected videos to intermediate videos that of mpg format
# ffmpeg -i input1.avi -qscale:v 1 intermediate1.mpg
# ffmpeg -i input2.avi -qscale:v 1 intermediate2.mpg
VIDEO_LIST=$(ls ${VIDEO_DIR}/*.mp4)
echo ${VIDEO_LIST}

for VID in ${VIDEO_LIST}; do
  FILE_NAME=$(basename -s ".mp4" ${VID})
  echo ${FILE_NAME}
  ./ffmpeg -i ${VID} -qscale:v 1 ${INTER_DIR}/${FILE_NAME}.mpg
done

# 2. Join all files together in a kind of a train/sausage file
# cat intermediate1.mpg intermediate2.mpg > intermediate_all.mpg

INTER_LIST=$(ls ${INTER_DIR}/*.mpg)
cat ${INTER_LIST} > ${OUTPUT_DIR}/final.mpg

# 3. Encode train/sausage file into a FullHD mp4 file
# ffmpeg -i intermediate_all.mpg -qscale:v 2 output.avi

./ffmpeg -i ${OUTPUT_DIR}/final.mpg \
  -r 25 \
  -c:v libx264 -preset fast -crf 25 -f mp4 \
  -c:a aac \
  ${OUTPUT_DIR}/final-fullhd.mp4

# 4. Scale the video down to HD resolution to be played back on Raspberry Pi

./ffmpeg -i ${OUTPUT_DIR}/final.mpg \
  -r 25 -vf scale=1280x720 \
  -c:v libx264 -preset fast -crf 25 -f mp4 \
  -profile baseline -pix_fmt yuv420p \
  -c:a aac \
  ${OUTPUT_DIR}/final-hd.mp4
